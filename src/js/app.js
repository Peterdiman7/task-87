import "../scss/app.scss";
import * as R from 'ramda';

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  // const R = require('ramda');

  var getClasses = R.pluck('class');

  const arrayToPluck = [
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ];
  let myVar = getClasses([
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ]);
  const articles = document.querySelectorAll("article");
  
  articles[0].classList.add(myVar[0]);
  articles[1].classList.add(myVar[1]);
  articles[2].classList.add(myVar[2]);

});
